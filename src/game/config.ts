import BootScene from '../game/scenes/bootScene';

export default {
    title: 'Math Defense',
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'phaser-example',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: 1024,
        height: 768
    },
    pixelArt: true,
    backgroundColor: '#000000',
    scene: BootScene
};
