import {SceneEvents} from '../scenes/levelScene';

export interface TowerConfig {
    name: string;
    texture: string;
    icon: string
    speed: integer; // interval between attacks in milliseconds
    damage: integer;
    radius: integer;
    cost: integer;
}

const grunt = { name: "grunt", texture: "grunt", icon: "assets/towers/grunt-icon.png", speed: 1500, damage: 2, radius: 2, cost: 100};
const knight = { name: "knight", texture:"soldier-with-sword", icon: "assets/towers/soldier-icon.png", speed: 1500, damage: 4, radius: 2, cost: 200};
const marine = { name: "marine", texture: "marine", icon: "assets/towers/marine-icon.png", speed: 1333, damage: 4, radius: 3, cost: 500};

export const TowerConfigs: TowerConfig[] = [grunt, knight, marine];

export default class Tower extends Phaser.GameObjects.Sprite {
    private config: TowerConfig
    private tileX: integer
    private tileY: integer
    private timer: Phaser.Time.TimerEvent;

    constructor(scene: Phaser.Scene, tileX: integer, tileY: integer, config: TowerConfig) {
        super(scene, tileX*32+16, tileY*32+16, config.texture)
        this.config = config
        scene.add.existing(this);

        scene.add.circle(this.x, this.y, this.config.radius*32, 0xffffff, 0.2);

        this.timer = this.scene.time.addEvent({ delay: this.config.speed, callback: this.attemptFire, callbackScope: this, loop: true })
        this.scene.events.on(SceneEvents.PAUSE, this.onPause, this);
        this.scene.events.on(SceneEvents.RESUME, this.onResume, this);
        this.depth = this.y + this.height;
    }

    public destroy(fromScene?: boolean): void {
        this.timer.destroy();
        if (this.scene) {
            this.scene.events.removeListener(SceneEvents.PAUSE, this.onPause, this);
            this.scene.events.removeListener(SceneEvents.RESUME, this.onResume, this);
        }
        super.destroy(fromScene);
    }

    public onPause() {
        this.timer.paused = true;
        // console.log("pause - tower");
    }

    public onResume() {
        this.timer.paused = false;
        // console.log("resume - tower");
    }

    public targetArea() {
        return new Phaser.Geom.Circle(this.x, this.y, this.config.radius*32);
    }

    public damage() {
        return this.config.damage;
    }

    public playAttack() {
        this.play(`${this.config.name}-attack`);
    }

    private attemptFire(event) {
        this.scene.events.emit(SceneEvents.TOWER_FIRE, this);
    }
}
