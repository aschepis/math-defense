import {SceneEvents} from "../scenes/levelScene"

interface WaveConfig {
    delay: integer
    enemies: string[]
    interval: integer
    warningMessage: string
    startMessage: string
    bonus: integer
}

interface LevelWavesConfig {
    waves: WaveConfig[]
}

export enum SpawnerState {
    PREPARING,
    IN_WAVE,
    COMPLETED
}
export default class WaveSpawner {
    private scene: Phaser.Scene
    private level: string;
    private cacheKey: string;
    private levelWaves: LevelWavesConfig;
    private currentWave: integer;
    private currentEnemy: integer;
    private timer: Phaser.Time.TimerEvent;
    public state: SpawnerState

    constructor(scene: Phaser.Scene, level: string) {
        this.scene = scene;
        this.level = level
        this.levelWaves = this.scene.game.cache.json.get(`${level}-waves`) as LevelWavesConfig;
    }

    public start() {
        if (this.timer != null) {
            this.timer.destroy();
        }
        this.currentWave = 0;
        this.initiateWave(this.currentWave);
    }

    public initiateWave(wave: integer) {
        this.currentEnemy = 0;
        const waveConfig = this.levelWaves.waves[wave];
        this.scene.events.emit(SceneEvents.SHOW_TEXT, waveConfig.warningMessage);
        this.state = SpawnerState.PREPARING;
        const seconds = waveConfig.delay / 1000;
        this.scene.events.emit(SceneEvents.UPDATE_COUNTDOWN, seconds);
        this.timer = this.scene.time.addEvent({
            delay: 1000,
            callback: this.updateCountdown,
            callbackScope: this,
            repeat: seconds - 1
        })

    }

    public updateCountdown() {
        this.scene.events.emit(SceneEvents.UPDATE_COUNTDOWN, this.timer.repeatCount);
        if (this.timer.repeatCount == 0) {
            this.startWave();
        }
    }

    public initiateNextWave() {
        this.currentWave++;
        if (this.currentWave >= this.levelWaves.waves.length) {
            this.state = SpawnerState.COMPLETED;
        } else {
            this.initiateWave(this.currentWave);
        }
    }

    private startWave() {
        this.state = SpawnerState.IN_WAVE;
        const waveConfig = this.levelWaves.waves[this.currentWave];
        this.scene.events.emit(SceneEvents.SHOW_TEXT, waveConfig.startMessage);
        this.timer = this.scene.time.addEvent({delay: waveConfig.interval, callback: this.spawnNextEnemy, callbackScope: this})
    }

    public timerRemainingSeconds(): number {
        if (this.timer == null) {
            return 0;
        }
        return this.timer.getOverallRemainingSeconds();
    }

    public pause() {
        if (this.timer != null) {
            this.timer.paused = true;
        }
    }

    public resume() {
        if (this.timer != null) {
            this.timer.paused = false;
        }
    }

    public stop() {
        if (this.timer != null) {
            this.timer.destroy();
        }
    }

    public spawnNextEnemy() {
        const waveConfig = this.levelWaves.waves[this.currentWave];
        if (this.currentEnemy >= waveConfig.enemies.length) {
            this.timer = this.scene.time.addEvent({delay: waveConfig.interval, callback: this.initiateNextWave, callbackScope: this})
        } else {
            this.scene.events.emit(SceneEvents.SPAWN_ENEMY, waveConfig.enemies[this.currentEnemy], this.currentWave, this.levelWaves.waves[this.currentWave].bonus);
            this.currentEnemy++;
            this.timer = this.scene.time.addEvent({delay: waveConfig.interval, callback: this.spawnNextEnemy, callbackScope: this})
        }
    }
}