import Phaser from 'phaser';
import {SceneEvents} from '../scenes/levelScene';

interface EnemyConfig {
    name: string;
    texture: string;
    icon: string;
    health: integer;
    speed: integer;
    reward: integer;
    renderOffsetY: integer;
}

const zombie = { name: "zombie", texture: "zombie-and-skeleton", icon: "assets/goblinsword.png", health: 10, speed: 40, reward: 25, renderOffsetY: 0 };
const skeleton = { name: "skeleton", texture: "zombie-and-skeleton", icon: "assets/goblinsword.png", health: 20, speed: 30, reward: 50, renderOffsetY: 0 };
const goblin = { name: "goblin", texture: "goblin", icon: "assets/goblinsword.png", health: 40, speed: 25, reward: 100, renderOffsetY: 0 };
const golem = { name: "golem", texture: "golem", icon: "assets/goblinsword.png", health: 100, speed: 20, reward: 250, renderOffsetY: 0 };
const dragon = { name: "dragon", texture: "dragon", icon: "assets/dragon.png", health: 1000, speed: 20, reward: 2500, renderOffsetY: -55 };

export const EnemyConfigs: EnemyConfig[] = [zombie, skeleton, goblin, golem, dragon];

const healthBarOffset = 25;

export default class Enemy extends Phaser.GameObjects.PathFollower {
    private config: EnemyConfig
    private currentAnimation: string
    private currentHealth: integer
    private healthBar: Phaser.GameObjects.Text;

    constructor(scene: Phaser.Scene, path: Phaser.Curves.Path, config: EnemyConfig) {
        super(scene, path, path.startPoint.x, path.startPoint.y + config.renderOffsetY, config.texture)
        this.config = config
        this.currentHealth = this.config.health;

        scene.add.existing(this);
        this.healthBar = scene.add.text(path.startPoint.x, path.startPoint.y - healthBarOffset, `${this.currentHealth}`)
        this.healthBar.setOrigin(0.5, 0.5);

        this.scene.events.on(SceneEvents.PAUSE, this.onPause, this);
        this.scene.events.on(SceneEvents.RESUME, this.onResume, this);
    }

    private onPause(event) {
        this.pauseFollow();
    }

    private onResume(event) {
        this.resumeFollow();
    }

    public go() {
        // speed is calculated in "distance per second". If a path is 1000 long then a speed of 20 would
        // mean it should take 50 seconds to cross that path.
        const duration = this.path.getLength() / this.config.speed;
        console.error(`duration for ${this.config.name} will be ${duration} seconds. (length: ${this.path.getLength()}, speed: ${this.config.speed})`)
        this.startFollow({
            duration: duration * 1000,
            onUpdate: this.onUpdate.bind(this),
            onComplete: this.onComplete.bind(this)
        });
        this.play(`${this.config.name}-walk-left`);
    }

    public onComplete(timeline: Phaser.Tweens.Timeline, param: any) {
        this.scene.events.emit(SceneEvents.ENEMY_REACHED_TOWER, this);
        this.destroy();
    }

    public onUpdate(tween: Phaser.Tweens.Tween, target: any, param: any) {
        // sometimes a final update occurs after death.. 🤷‍♂️
        if (this.currentHealth <= 0) {
            console.log("update from beyond the grave");
            return;
        }

        const depth = this.y + this.height;
        this.depth =depth;
        this.healthBar.depth = depth;

        // update health bar
        this.healthBar.text = `${this.currentHealth}`;
        this.healthBar.setPosition(this.x, this.y - healthBarOffset);

        // update animation
        const tan = this.path.getTangent(tween.getValue());
        let anim = `${this.config.name}-walk-left`;

        if ((tan.angle() >= 2.4) && (tan.angle() <= 4))
            anim = `${this.config.name}-walk-left`;

        if (((tan.angle() >= 0) && (tan.angle() <= 0.8)) || ((tan.angle() >= 5.6) && (tan.angle() <= 6.4)))
            anim = `${this.config.name}-walk-right`;

        if ((tan.angle() > 4) && (tan.angle() < 5.6))
            anim = `${this.config.name}-walk-up`;

        if ((tan.angle() > 0.8) && (tan.angle() < 2.4))
            anim = `${this.config.name}-walk-down`;

        if ((this.currentAnimation != anim)) {
            this.currentAnimation = anim
            this.play(anim);
        }
    }

    public move(direction: string) {
        switch (direction) {
            case "left":
                this.body.velocity.x = -1 * this.config.speed;
                this.body.velocity.y = 0;
                this.play(`${this.config.name}-walk-left`);
                break;
            case "right":
                this.body.velocity.x = this.config.speed;
                this.body.velocity.y = 0;
                this.play(`${this.config.name}-walk-right`);
                break;
            case "up":
                this.body.velocity.x = 0
                this.body.velocity.y = -1 * this.config.speed;
                this.play(`${this.config.name}-walk-up`);
                break;
            case "down":
                this.body.velocity.x = 0
                this.body.velocity.y = this.config.speed;
                this.play(`${this.config.name}-walk-down`);
                break;
            default:
                console.log("unhandled direction " + direction)
                break;
        }
    }

    public reward() {
        return this.config.reward;
    }

    public destroy(fromScene?: boolean): void {
        this.stopFollow();
        this.healthBar.destroy();
        if (this.scene) {
            this.scene.events.removeListener(SceneEvents.PAUSE, this.onPause, this);
            this.scene.events.removeListener(SceneEvents.RESUME, this.onResume, this);
        }
        super.destroy(fromScene);
}

    public takeDamage(damage: integer) {
        this.currentHealth -= damage;
        if (this.currentHealth <= 0) {
            this.scene.events.emit(SceneEvents.ENEMY_DESTROYED, this);
            this.destroy();
        }
    }
}