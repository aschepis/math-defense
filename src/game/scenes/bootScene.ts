import Phaser, { Time } from "phaser";
import GameEvents from "../../events/GameEvents";
import LevelScene from "./levelScene";
import MainMenuScene from "./mainMenuScene";

export default class BootScene extends Phaser.Scene {
    constructor() {
        super("boot-scene");
    }

    preload(): void {
        this.load.setBaseURL('./assets');
        this.load.image('tiles', 'tiles.png');
        this.load.image('icons', 'icons.png');
        this.load.spritesheet('zombie-and-skeleton', 'enemies/zombie_n_skeleton2.png', { frameWidth: 32, frameHeight: 64 });
        this.load.spritesheet('goblin', 'enemies/goblinsword.png', { frameWidth: 64, frameHeight: 64 });
        this.load.spritesheet('golem', 'enemies/golem-walk.png', { frameWidth: 64, frameHeight: 64 });
        this.load.spritesheet('dragon', 'enemies/dragon.png', { frameWidth: 185, frameHeight: 185 });
        this.load.spritesheet('soldier-with-sword', 'towers/Soldier 02-4.png', { frameWidth: 32, frameHeight: 32 });
        this.load.spritesheet('grunt', 'towers/grunt.png', { frameWidth: 32, frameHeight: 32 });
        this.load.spritesheet('marine', 'towers/marine2.png', { frameWidth: 32, frameHeight: 42 });

        this.load.image('main-menu-title', 'main-menu/title.png');
        this.load.image('main-menu-start', 'main-menu/start.png');
        this.load.image('main-menu-quit', 'main-menu/quit.png');
    }

    create(): void {
        this.makeAnimations();

        this.game.events.on(GameEvents.START_LEVEL, (level) => {
            this.scene.add(level, new LevelScene(level), true)
        })
        this.scene.add('main-menu', new MainMenuScene(), true)
    }

    private makeAnimations() {
        this.anims.create({ key: "skeleton-walk-down", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [3, 4, 5, 4] }), repeat: -1 });
        this.anims.create({ key: "skeleton-walk-up", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [30, 31, 32, 31] }), repeat: -1 });
        this.anims.create({ key: "skeleton-walk-left", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [12, 13, 14, 13] }), repeat: -1 });
        this.anims.create({ key: "skeleton-walk-right", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [21, 22, 23, 22] }), repeat: -1 });

        this.anims.create({ key: "zombie-walk-down", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [0, 1, 2, 1] }), repeat: -1 });
        this.anims.create({ key: "zombie-walk-up", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [27, 28, 29, 28] }), repeat: -1 });
        this.anims.create({ key: "zombie-walk-left", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [9, 10, 11, 10] }), repeat: -1 });
        this.anims.create({ key: "zombie-walk-right", frameRate: 4, frames: this.anims.generateFrameNumbers("zombie-and-skeleton", { frames: [18, 19, 20, 19] }), repeat: -1 });

        this.anims.create({ key: "goblin-walk-down", frameRate: 4, frames: this.anims.generateFrameNumbers("goblin", { frames: [0, 1, 2, 3, 4, 5, 6] }), repeat: -1 });
        this.anims.create({ key: "goblin-walk-up", frameRate: 4, frames: this.anims.generateFrameNumbers("goblin", { frames: [24, 25, 26, 27, 28, 29, 30] }), repeat: -1 });
        this.anims.create({ key: "goblin-walk-left", frameRate: 4, frames: this.anims.generateFrameNumbers("goblin", { frames: [36, 37, 38, 39, 40, 41, 42] }), repeat: -1 });
        this.anims.create({ key: "goblin-walk-right", frameRate: 4, frames: this.anims.generateFrameNumbers("goblin", { frames: [12, 13, 14, 15, 16, 17, 18] }), repeat: -1 });

        this.anims.create({ key: "golem-walk-down", frameRate: 4, frames: this.anims.generateFrameNumbers("golem", { frames: [14, 15, 16, 17, 18, 19, 20] }), repeat: -1 });
        this.anims.create({ key: "golem-walk-up", frameRate: 4, frames: this.anims.generateFrameNumbers("golem", { frames: [0, 1, 2, 3, 4, 5, 6] }), repeat: -1 });
        this.anims.create({ key: "golem-walk-left", frameRate: 4, frames: this.anims.generateFrameNumbers("golem", { frames: [7, 8, 9, 10, 11, 12, 13] }), repeat: -1 });
        this.anims.create({ key: "golem-walk-right", frameRate: 4, frames: this.anims.generateFrameNumbers("golem", { frames: [21, 22, 23, 24, 25, 26, 27] }), repeat: -1 });

        this.anims.create({ key: "dragon-walk-down", frameRate: 8, frames: this.anims.generateFrameNumbers("dragon", { frames: [15,16,17,16] }), repeat: -1 });
        this.anims.create({ key: "dragon-walk-up", frameRate: 8, frames: this.anims.generateFrameNumbers("dragon", { frames: [15,16,17,16] }), repeat: -1 });
        this.anims.create({ key: "dragon-walk-left", frameRate: 12, frames: this.anims.generateFrameNumbers("dragon", { frames: [19,18,17,16,15,24,23,22,21,20] }), repeat: -1 });
        this.anims.create({ key: "dragon-walk-right", frameRate: 12, frames: this.anims.generateFrameNumbers("dragon", { frames: [0,1,2,3,4,5,6,7,8,9] }), repeat: -1 });

        this.anims.create({ key: "knight-attack", frameRate: 12, frames: this.anims.generateFrameNumbers("soldier-with-sword", { frames: [0,3,6,9,0] }), repeat: 0});
        this.anims.create({ key: "grunt-attack", frameRate: 12, frames: this.anims.generateFrameNumbers("grunt", { frames: [0,3,6,9,0] }), repeat: 0});
        this.anims.create({ key: "marine-attack", frameRate: 12, frames: this.anims.generateFrameNumbers("marine", { frames: [0,1,2] }), repeat: 5});
    }

}