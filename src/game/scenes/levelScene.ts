import { getTableHeadUtilityClass } from "@mui/material";
import { Scene } from "phaser";
import GameEvents from "../../events/GameEvents";
import Enemy, { EnemyConfigs } from "../classes/Enemy";
import Tower from "../classes/Tower";
import WaveSpawner, { SpawnerState } from "../classes/WaveSpawner";
import config from '../config';

export const SceneEvents = {
    PAUSE: 'PAUSE',
    RESUME: 'RESUME',
    GAME_OVER: 'GAME_OVER',
    // TOWER_FIRE is triggered when a tower is ready to fire on
    // enemies if any are in range.l
    TOWER_FIRE: 'TOWER_FIRE',
    // ENEMY_DESTROYED is triggered when an enemy has reached zero
    // hp
    ENEMY_DESTROYED: 'ENEMY_DESTROYED',
    // ENEMY_REACHED_TOWER is triggered when an enemy has reached
    // the player's tower.
    ENEMY_REACHED_TOWER: 'ENEMY_REACHED_TOWER',
    SPAWN_ENEMY: 'SPAWN_ENEMY',
    SHOW_TEXT: 'SHOW_TEXT',
    UPDATE_COUNTDOWN: 'UPDATE_COUNTDOWN'
}

type EnemyEntry = {
    enemy: Enemy
    wave: integer
    waveBonus: integer
}

export default class LevelScene extends Phaser.Scene {
    private square: Phaser.GameObjects.Rectangle & { body: Phaser.Physics.Arcade.Body };
    private spawnTimer: Phaser.Time.TimerEvent;
    private level: string;
    private key: string;
    private map: Phaser.Tilemaps.Tilemap;
    private path: Phaser.Curves.Path;
    private pointerRect: Phaser.GameObjects.Rectangle;
    private waveSpawner: WaveSpawner
    private zoom: number;

    private loaded: boolean = false;
    private levelCompleted = false;
    private enemies: EnemyEntry[] = [];
    private towers: Tower[] = [];

    private moneyText: Phaser.GameObjects.Text;
    private healthText: Phaser.GameObjects.Text;
    private countDownText: Phaser.GameObjects.Text;
    private statusText: Phaser.GameObjects.Text;

    private money: integer;
    private paused: boolean;
    private towerHealth: integer;

    constructor(level: string) {
        super({
            active: false,
            visible: false,
            key: level,
        });

        this.level = level
    }

    public preload() {
        console.log(`PRELOAD ${this.level}`);
        this.load.setBaseURL('./assets');
        this.load.tilemapTiledJSON(`${this.level}-map`, `levels/${this.level}.json`);
        this.load.json(`${this.level}-waves`, `levels/${this.level}-waves.json`);
    }

    public create() {
        this.reset();

        this.loadMap();
        this.configureMouse();
        this.createUI();

        this.game.events.on(GameEvents.ON_PLACE_TOWER, this.onPlaceTower, this)
        this.game.events.on(GameEvents.PLACE_TOWER, this.placeTower, this)
        this.game.events.on(GameEvents.CANCEL_PLACE_TOWER, this.cancelPlaceTower, this)
        this.game.events.on(GameEvents.RESTART_LEVEL, this.restartLevel, this)

        this.events.on(SceneEvents.ENEMY_DESTROYED, this.enemyDestroyed, this);
        this.events.on(SceneEvents.ENEMY_REACHED_TOWER, this.enemyReachedTower, this);
        this.events.on(SceneEvents.TOWER_FIRE, this.towerFire, this)
        this.events.on(SceneEvents.SHOW_TEXT, this.showText, this)
        this.events.on(SceneEvents.SPAWN_ENEMY, this.spawnEnemy, this)
        this.events.on(SceneEvents.UPDATE_COUNTDOWN, this.updateCountdown, this)

        this.waveSpawner.start();

        this.events.on(SceneEvents.PAUSE, (event) => {
            this.paused = true;
            this.waveSpawner.pause();
        })

        this.events.on(SceneEvents.RESUME, (event) => {
            this.paused = false;
            this.waveSpawner.resume();
        })

        this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
        const widthRatio = config.scale.width / this.map.widthInPixels;
        const heightRatio = config.scale.height / this.map.heightInPixels;

        this.zoom = widthRatio > heightRatio ? heightRatio : widthRatio;
        this.cameras.main.zoom = this.zoom;
    }

    private createUI() {
        const textStyle: Phaser.Types.GameObjects.Text.TextStyle = {
            align: "right",
            fixedWidth: 295
        }

        const largeTextStyle: Phaser.Types.GameObjects.Text.TextStyle = {
            align: "center",
            fontSize: "14pt",
        }

        const uiLeft = this.map.widthInPixels - 300;
        const uiBottom = this.map.heightInPixels;

        this.moneyText = this.add.text(uiLeft, uiBottom - 30, `Money: $${this.money}`, textStyle);
        this.moneyText.depth = 1000;

        this.healthText = this.add.text(uiLeft, uiBottom - 48, `Health: ${this.towerHealth}`, textStyle);
        this.healthText.depth = 1000;

        this.countDownText = this.add.text(uiLeft, uiBottom - 64, '', textStyle);
        this.countDownText.depth = 1000;

        this.statusText = this.add.text(this.map.widthInPixels / 2, this.map.heightInPixels / 2, '', largeTextStyle).setOrigin(0.5);
        this.statusText.depth = 1000;
    }

    public restartLevel() {
        this.reset();
        this.scene.restart()
    }

    public clearStatusText() {
        this.statusText.text = "";
    }
    public showText(text) {
        this.statusText.text = text;
        this.time.addEvent({delay: 2000, callback: this.clearStatusText, callbackScope: this})
    }

    public towerFire(tower) {
        const targetArea = tower.targetArea();
        for(const entry of this.enemies) {
            const enemy = entry.enemy;
            const enemyBounds = enemy.getBounds();
            if(Phaser.Geom.Intersects.CircleToRectangle(targetArea, enemyBounds)) {
                tower.playAttack();
                const damage = tower.damage()
                enemy.takeDamage(damage);
                this.money += damage;
                break;
            }
        }
    }

    public onPlaceTower(event) {
        this.events.emit(SceneEvents.PAUSE);
    }

    public placeTower(event) {
        const tower = new Tower(this, event.pos.x, event.pos.y, event.config);
        this.towers.push(tower);
        this.money -= event.config.cost;
        this.events.emit(SceneEvents.RESUME);
    }

    public cancelPlaceTower(event) {
        this.events.emit(SceneEvents.RESUME);
    }

    public enemyDestroyed(enemy) {
        const entry = this.enemies.find((e) => { return e.enemy == enemy});
        this.enemies = this.enemies.filter((e) => { return e.enemy != enemy});
        this.money += enemy.reward();

        // is this the end of a wave? if so give the wave bonus
        if (this.enemies.find((e) => e.wave == entry.wave) == null) {
            this.events.emit(SceneEvents.SHOW_TEXT, `Wave Bonus: $${entry.waveBonus}`)
            this.money += entry.waveBonus;
        }
    }

    public enemyReachedTower(enemy) {
        const entry = this.enemies.find((e) => { return e.enemy == enemy});
        this.enemies = this.enemies.filter((e) => { return e.enemy != enemy});

        // is this the end of a wave? if so give the wave bonus
        if (this.enemies.find((e) => e.wave == entry.wave) == null) {
            this.events.emit(SceneEvents.SHOW_TEXT, `Wave Bonus: $${entry.waveBonus}`)
            this.money += entry.waveBonus;
        }

        this.towerHealth -= enemy.currentHealth;
        if (this.towerHealth <= 0) {
            this.events.emit(SceneEvents.PAUSE);
            this.game.events.emit(GameEvents.GAME_OVER, this);
        }
    }

    private reset() {
        this.towers.forEach(t => t.destroy());
        this.enemies.forEach(e => e.enemy.destroy());
        this.towers = [];
        this.enemies = [];
        if (this.waveSpawner != null) {
            this.waveSpawner.stop();
        }

        this.levelCompleted = false;
        this.waveSpawner = new WaveSpawner(this, this.level);

        if (this.path != null) {
            this.path.destroy();
            this.path = null;
        }

        this.game.events.removeListener(GameEvents.ON_PLACE_TOWER, this.onPlaceTower, this)
        this.game.events.removeListener(GameEvents.PLACE_TOWER, this.placeTower, this)
        this.game.events.removeListener(GameEvents.CANCEL_PLACE_TOWER, this.cancelPlaceTower, this)
        this.game.events.removeListener(GameEvents.RESTART_LEVEL, this.restartLevel, this)

        this.events.removeListener(SceneEvents.ENEMY_DESTROYED, this.enemyDestroyed, this);
        this.events.removeListener(SceneEvents.ENEMY_REACHED_TOWER, this.enemyReachedTower, this);
        this.events.removeListener(SceneEvents.TOWER_FIRE, this.towerFire, this)
        this.events.removeListener(SceneEvents.SHOW_TEXT, this.showText, this)
        this.events.removeListener(SceneEvents.SPAWN_ENEMY, this.spawnEnemy, this)
        this.events.removeListener(SceneEvents.UPDATE_COUNTDOWN, this.updateCountdown, this)

        this.paused = false;
        this.towerHealth = 100;
        this.money = 500;
    }

    private updateCountdown(secondsRemaining: integer) {
        if (secondsRemaining <= 0) {
            this.countDownText.text = ``;
        } else {
            this.countDownText.text = `Next Wave: ${secondsRemaining}s`;
        }
    }

    private configureMouse() {
        this.input.on("pointerup", (event) => {
            const pointer = this.input.activePointer;
            const x = Math.round((pointer.x-16) / 32 / this.zoom);
            const y = Math.round((pointer.y-16) / 32 / this.zoom);
            if (!this.paused && this.placeableSquare(x, y)) {
                this.game.events.emit(GameEvents.ON_PLACE_TOWER, {x: x, y: y, money: this.money});
            }
        })

        this.pointerRect = this.add.rectangle(0, 0, 32, 32, 0xffff00, 0x888888);
        this.pointerRect.z = 1000;
    }

    private loadMap() {
        this.map = this.make.tilemap({ key: `${this.level}-map` });
        const tiles = this.map.addTilesetImage('tiles', 'tiles');
        const icons = this.map.addTilesetImage('icons', 'icons');
        const groundLayer = this.map.createLayer("Ground", tiles, 0, 0);
        const aboveGroundLayer = this.map.createLayer("Above Ground", tiles, 0, 0);
        const placeable = this.map.createLayer("Placeable", icons, 0, 0);
        const objectLayer = this.map.getObjectLayer("Objects");

        objectLayer.objects.forEach((obj) => {
            if (this.path == null) {
                this.path = this.add.path(obj.x + 16, obj.y - 32);
            } else {
                this.path.lineTo(obj.x + 16, obj.y - 32);
            }
        });
    }

    private spawnEnemy(kind: string, wave: integer, waveBonus: integer) {
        // find spawn point
        const entrance = this.map.getObjectLayer("Objects").objects.find((obj) => obj.type == "entrance");
        const direction = entrance.properties.find((p) => p.name == "direction")

        const enemyConfig = EnemyConfigs.find(e => e.name == kind)
        const enemy = new Enemy(this, this.path, enemyConfig);

        this.enemies.push({enemy, wave, waveBonus});
        enemy.go();
    }

    public update() {
        this.updateUI();
        if (!this.levelCompleted && this.enemies.length == 0 && this.waveSpawner.state == SpawnerState.COMPLETED) {
            this.levelCompleted = true;
            this.statusText.text = "LEVEL COMPLETED!"
            this.time.addEvent({ delay: 5000, callback: this.gotoNextLevel, callbackScope: this });
        }
    }

    private gotoNextLevel() {
        this.cameras.main.fadeOut(1000, 0, 0, 0)
        this.cameras.main.once(Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE, (cam, effect) => {
            const levelId = parseInt(this.level.match(/[0-9]+$/)[0])
            const nextLevel = `level${levelId+1}`;
            this.game.events.emit(GameEvents.START_LEVEL, nextLevel);
        });
	}

    private placeableSquare(x: integer, y: integer): boolean {
        const layer = this.map.getLayer("Placeable");
        if (x >= layer.width || y >= layer.height) {
            return false;
        }
        const placeableSquare = layer.data[y][x].index != -1;
        const occupied = this.towers.find((t) => t.x == x*32+16 && t.y == y*32+16) != null;
        return placeableSquare && !occupied;
    }

    private updateUI() {
        const pointer = this.input.activePointer;

        const x = Math.round((pointer.x-16) / 32 / this.zoom);
        const y = Math.round((pointer.y-16) / 32 / this.zoom);
        this.pointerRect.x = x * 32 + 16;
        this.pointerRect.y = y * 32 + 16;

        const placeableSquare = this.placeableSquare(x, y);
        this.pointerRect.fillColor = placeableSquare ? 0xffff00 : 0xff0000;
        this.pointerRect.fillAlpha = 0.5;

        this.moneyText.text = `$${this.money}`;
        this.healthText.text = `Health: ${this.towerHealth}`;
    }
}