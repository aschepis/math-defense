import GameEvents from "../../events/GameEvents";


export default class MainMenuScene extends Phaser.Scene {
    constructor() {
        super({
            active: false,
            visible: false,
            key: "main-menu",
        });
    }

    create(): void {
        const gameSize = this.game.scale.gameSize;
        const titleSprite = this.add.sprite(gameSize.width / 2, gameSize.height / 6, 'main-menu-title');
        titleSprite.originX = 0.5

        const startSprite = this.add.sprite(gameSize.width / 2, gameSize.height / 6 + 300, 'main-menu-start').setInteractive();
        startSprite.originX = 0.5

        const quitSprite = this.add.sprite(gameSize.width / 2, gameSize.height / 6 + 400, 'main-menu-quit').setInteractive();
        quitSprite.originX = 0.5

        startSprite.on('pointerdown', () => {this.tintSprite(startSprite)});
        quitSprite.on('pointerdown', () => {this.tintSprite(quitSprite)});
        startSprite.on('pointerout', () => {this.clearTint(startSprite)});
        quitSprite.on('pointerout', () => {this.clearTint(quitSprite)});
        startSprite.on('pointerup', () => {this.startGame(startSprite)});
        quitSprite.on('pointerup', () => {this.quitGame(quitSprite)});
    }

    private tintSprite(sprite: Phaser.GameObjects.Sprite) {
        sprite.setAlpha(0.5);
    }

    private clearTint(sprite: Phaser.GameObjects.Sprite) {
        sprite.setAlpha(1.0);
    }

    private startGame(sprite: Phaser.GameObjects.Sprite) {
        this.clearTint(sprite)
        this.game.events.emit(GameEvents.START_LEVEL, "level1");
        this.scene.remove();
    }

    private quitGame(sprite: Phaser.GameObjects.Sprite) {
        this.clearTint(sprite)
        alert("you can't quit the browser, silly! Just close the tab");
    }
}