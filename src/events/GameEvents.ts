export default {
    // ON_PLACE_TOWER is emitted when the user clicks on a square
    // to place a tower
    ON_PLACE_TOWER: 'ON_PLACE_TOWER',
    // PLACE_TOWER is emitted when the user has selected a tower
    // to place on a particular square (via the select tower
    // ui)
    PLACE_TOWER: 'PLACE_TOWER',
    // CANCEL_PLACE_TOWER is emitted when the user has canceled the
    // select tower ui
    CANCEL_PLACE_TOWER: 'CANCEL_PLACE_TOWER',
    // SHOW_MATH_PROBLEM is triggered any time we want the player
    // to answer a math problem.
    SHOW_MATH_PROBLEM: 'SHOW_MATH_PROBLEM',
    // GAME_OVER is triggered when the player's tower reaches
    // zero HP and is destroyed.
    GAME_OVER: 'GAME_OVER',
    // START_LEVEL is triggered to signal that a level should be started
    START_LEVEL: 'START_LEVEL',
    // RESTART_LEVEL is triggered when the player chooses to
    // restart the current level
    RESTART_LEVEL: 'RESTART_LEVEL',
    // QUIT_TO_MENU is triggered when the player chooses to
    // return to the main menu
    QUIT_TO_MENU: 'QUIT_TO_MENU'
  }