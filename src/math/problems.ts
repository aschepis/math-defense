export interface MathProblem {
    text: string;
    answer: integer;
}

let Problems: MathProblem[] = [];
for(let x = 0; x < 13; x++) {
    for (let y = x; y < 13; y++) {
        Problems.push({text: `${x} * ${y} = `, answer: x * y});
    }
}

export default Problems