import React from 'react';
import config from '../game/config';
import './App.css';

import { GameComponent } from 'phaser-react-tools';
import SelectTowerDialog from './SelectTowerDialog';
import MathProblemDialog from './MathProblemDialog';
import GameOverDialog from './GameOverDialog';

function App() {
    return (
        <>
        <GameComponent config={config}>
            <SelectTowerDialog></SelectTowerDialog>
            <MathProblemDialog></MathProblemDialog>
            <GameOverDialog></GameOverDialog>
        </GameComponent>
      </>
    );
}

export default App;
