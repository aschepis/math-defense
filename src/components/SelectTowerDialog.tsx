import { Button, Dialog, DialogTitle, DialogContentText, DialogActions, DialogContent, Grid, Paper, FormControlLabel, Radio, FormLabel, RadioGroup } from '@mui/material'
import { useEventEmitter, useEventListener } from 'phaser-react-tools';
import React, { useState, useEffect, ChangeEvent } from 'react'
import GameEvents from '../events/GameEvents';
import {TowerConfigs} from '../game/classes/Tower';
import { styled } from '@mui/material/styles';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(3),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

function SelectTowerDialog() {
    const [open, setOpen] = useState(false);
    const [targetPos, setTargetPos] = useState({x:0, y:0});
    const [selectedTower, setSelectedTower] = useState('');
    const [tooExpensive, setTooExpensive] = useState(false);
    const [money, setMoney] = useState(0);
    const emitPlaceTower = useEventEmitter(GameEvents.PLACE_TOWER);
    const emitCancelPlaceTower = useEventEmitter(GameEvents.CANCEL_PLACE_TOWER);
    const emitShowMathProblem = useEventEmitter(GameEvents.SHOW_MATH_PROBLEM);

    useEventListener(GameEvents.ON_PLACE_TOWER, ({x, y, money}) => {
        setTargetPos({x, y});
        setMoney(money);
        setSelectedTower(TowerConfigs[0].name);
        setOpen(true);
    })

    const handleClose = () => {
        const selectedConfig = TowerConfigs.find(t => t.name == selectedTower);
        const canPurchase = selectedConfig.cost <= money
        setTooExpensive(!canPurchase);
        if (canPurchase) {
            emitShowMathProblem({pos: targetPos, towerConfig: selectedConfig})
            setOpen(false);
        }
    };

    const handleCancel = (event) => {
        setOpen(false);
        emitCancelPlaceTower();
    };

    const selectTower = (event: ChangeEvent<HTMLInputElement>) => {
        setSelectedTower((event.target as HTMLInputElement).value)
    }

    return (
        <Dialog
            fullWidth={true}
            maxWidth={"md"}
            open={open}
            onClose={handleCancel}
            aria-labelledby="select-tower-dialog-title"
            aria-describedby="select-tower-dialog-description"
        >
            <DialogTitle id="select-tower-dialog-title">
                {"Select Tower"}
            </DialogTitle>
            <DialogContent dividers={true}>
                <DialogContentText id="select-tower-dialog-description">
                    <FormLabel id="select-a-tower-label">Select a Tower</FormLabel>
                    {tooExpensive ? <>You can't afford that!</> : <></>}
                </DialogContentText>
                <RadioGroup
                    aria-labelledby="select-a-tower-label"
                    defaultValue="grunt"
                    name="tower-select-radio-group"
                    onChange={selectTower}
                >
                    <Grid container spacing={3}>
                        {TowerConfigs.map((tower, i) => {
                            return (<Grid key={tower.name} item xs={2}>
                                <Item>
                                    <FormControlLabel value={tower.name} control={<Radio />} label={tower.name} />
                                    <img src={tower.icon} width="100%" height="100%" />
                                    ${tower.cost}
                                </Item>
                            </Grid>)
                        })}
                    </Grid>
                </RadioGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCancel}>Cancel</Button>
                <Button onClick={handleClose} type="submit" autoFocus>OK</Button>
            </DialogActions>
        </Dialog>
    )
}

export default SelectTowerDialog