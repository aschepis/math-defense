import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { useEventEmitter, useEventListener } from 'phaser-react-tools';
import React, { useState } from 'react';
import GameEvents from '../events/GameEvents';

function GameOverDialog() {
    const [open, setOpen] = useState(false);
    const [scene, setScene] = useState<Phaser.Scene>(null);

    const emitRestartLevel = useEventEmitter(GameEvents.RESTART_LEVEL);
    const emitQuitToMenu = useEventEmitter(GameEvents.QUIT_TO_MENU);

    useEventListener(GameEvents.GAME_OVER, (scene) => {
        setScene(scene);
        setOpen(true);
    });

    const handleRestart = () => {
        setOpen(false);
        emitRestartLevel(scene);
    };

    const handleQuit = (event) => {
        setOpen(false);
        emitQuitToMenu();
    };

    return (
        <Dialog
            fullWidth={true}
            maxWidth={"md"}
            open={open}
            onClose={handleQuit}
            aria-labelledby="game-over-dialog-title"
            aria-describedby="game-over-dialog-description"
        >
            <DialogTitle id="game-over-dialog-title">
                {"Game Over"}
            </DialogTitle>
            <DialogContent dividers={true}>
                Game Over
            </DialogContent>
            <DialogActions>
                <Button onClick={handleQuit}>Quit</Button>
                <Button onClick={handleRestart} type="submit">RestartLevel</Button>
            </DialogActions>
        </Dialog>
    )
}

export default GameOverDialog