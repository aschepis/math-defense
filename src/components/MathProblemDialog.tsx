import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@mui/material';
import { useEventEmitter, useEventListener } from 'phaser-react-tools';
import React, { ChangeEvent, KeyboardEvent, useState } from 'react';
import GameEvents from '../events/GameEvents';
import { TowerConfig } from '../game/classes/tower';
import Problems, { MathProblem } from '../math/problems';

function MathProblemDialog() {
    const [open, setOpen] = useState(false);
    const [targetPos, setTargetPos] = useState({x:0, y:0});
    const [towerConfig, setTowerConfig] = useState<TowerConfig>(null);
    const [problem, setProblem] = useState<MathProblem>(null);
    const [answer, setAnswer] = useState("");
    const [wrongAnswer, setWrongAnswer] = useState(false);

    const emitPlaceTower = useEventEmitter(GameEvents.PLACE_TOWER);
    const emitCancelPlaceTower = useEventEmitter(GameEvents.CANCEL_PLACE_TOWER);

    useEventListener(GameEvents.SHOW_MATH_PROBLEM, ({pos, towerConfig}) => {
        setTargetPos(pos);
        setTowerConfig(towerConfig as TowerConfig);

        const problemId = Math.floor(Math.random() * Problems.length);
        setProblem(Problems[problemId]);
        setAnswer("");
        setWrongAnswer(false);

        setOpen(true);
    })

    const handleClose = () => {
        if (Number(answer) == problem.answer) {
            setOpen(false);
            emitPlaceTower({pos: targetPos, config: towerConfig});
        } else {
            setWrongAnswer(true);
        }
    };

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        setAnswer(event.target.value);
    }

    const handleCancel = (event) => {
        setOpen(false);
        emitCancelPlaceTower();
    };

    const handleKeyUp = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key == "Enter") {
            handleClose();
        }
    }

    return (
        <Dialog
            fullWidth={true}
            maxWidth={"md"}
            open={open}
            onClose={handleCancel}
            aria-labelledby="math-problem-dialog-title"
            aria-describedby="math-problem-dialog-description"
        >
            <DialogTitle id="math-problem-dialog-title">
                {"Answer the problem"}
            </DialogTitle>
            <DialogContent dividers={true}>
                {problem ? problem.text : ""}
                <TextField id="math-problem-answer"
                    type="outlined-basic"
                    label="Answer"
                    variant="outlined"
                    inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                    value={answer}
                    onChange={handleChange}
                    onKeyUp={handleKeyUp}
                    error={wrongAnswer}
                    autoFocus
                    helperText={wrongAnswer ? "Incorrect answer" : ""}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCancel}>Cancel</Button>
                <Button onClick={handleClose} type="submit">OK</Button>
            </DialogActions>
        </Dialog>
    )
}

export default MathProblemDialog